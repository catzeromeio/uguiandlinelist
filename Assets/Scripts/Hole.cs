﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Hole : MonoBehaviour {

    int _ImageWidth;
    int _ImageHeight;
    Texture2D _Texture;
    Image _Image;

    public Vector2 Position;
    public int BlockWidth;
    public int BlockHeight;

    void Start()
    {
        _ImageWidth = Screen.width;
        _ImageHeight = Screen.height;
        _Image = GetComponent<Image>();
    }


    void DigAHole()
    {
        _Texture = new Texture2D(_ImageWidth, _ImageHeight, TextureFormat.ARGB32, false);

        //卸载驻留在内存中的纹理资源
        Resources.UnloadUnusedAssets();
       
        //将指定区块的像素透明度设置为0
        Color32[] colors = new Color32[BlockHeight * BlockWidth];
        for (int n = 0; n < colors.Length; ++n)
        {
            colors[n] = new Color32(255, 255, 255, 0);
        }
        _Texture.SetPixels32((int)Position.x, (int)Position.y, BlockWidth, BlockHeight, colors);
        _Texture.Apply();

        _Image.sprite = Sprite.Create(_Texture, new Rect(0, 0, _ImageWidth, _ImageHeight), new Vector2(0.5f, 0.5f));
    }

    // Update is called once per frame
    void Update()
    {
        //将透明区块限制在屏幕内
        if (BlockHeight < 0 || BlockWidth < 0 || Position.x < 0
            || Position.y < 0 || Position.y + BlockHeight > _ImageHeight
            || Position.x + BlockWidth > _ImageWidth)
            return;

        DigAHole();
    }
}
