﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//线段的顶点可以在Editor中设置，程序会根据顶点数据实时地进行绘制线段
public class LineList : MonoBehaviour
{
    MeshFilter meshFilter;
    Mesh mesh;
    //线宽
    float Width = 0.01f;

    int[] Triangles
    {
        get 
        {
            int[] triangles = new int[Lines.Count * 6];
            for (int index = 0; index < triangles.Length / 6; ++index)
            {
                //填充索引
                triangles[index * 6] = 4 * index + 0;
                triangles[index * 6 + 1] = 4 * index + 1;
                triangles[index * 6 + 2] = 4 * index + 2;
                triangles[index * 6 + 3] = 4 * index + 0;
                triangles[index * 6 + 4] = 4 * index + 2;
                triangles[index * 6 + 5] = 4 * index + 3;
            }

            return triangles;
        }
    }

    Vector3[] AllVertices
    {
        get
        {
            List<Vector3> allVertices = new List<Vector3>();
            foreach (Line line in Lines)
            {
                //填充顶点
                allVertices.Add(new Vector3(line.StartPoint.x, line.StartPoint.y - Width, line.StartPoint.z));
                allVertices.Add(new Vector3(line.StartPoint.x, line.StartPoint.y + Width, line.StartPoint.z));
                allVertices.Add(new Vector3(line.EndPoint.x, line.EndPoint.y + Width, line.EndPoint.z));
                allVertices.Add(new Vector3(line.EndPoint.x, line.EndPoint.y - Width, line.EndPoint.z));
            }

            return allVertices.ToArray();
        }
    }

    //线段的数量   
    public List<Line> Lines;
    
    void Start()
    {      
        meshFilter = GetComponent<MeshFilter>();
        mesh = meshFilter.mesh;
    }

    void Update()
    {
        //填充顶点信息
        mesh.vertices = AllVertices;
        //填充顶点索引
        mesh.triangles = Triangles;       
    }
}

[Serializable]
public struct Line
{
    public Vector3 StartPoint;
    public Vector3 EndPoint;

    public Line(Vector3 start, Vector3 end)
    {
        StartPoint = start;
        EndPoint = end;
    }
}